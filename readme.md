# Population service
Spring boot application for populating database

## Run locally
```
mvn package
java -jar -Dspring.profiles.active=local ./target/*.jar
```