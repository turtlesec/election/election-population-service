package no.election.populationservice

import no.election.populationservice.service.ElectionService
import no.election.populationservice.service.ProtocolsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController()
@RequestMapping("api")
class Controller {
    @Autowired
    lateinit var service: ElectionService

    @Autowired
    lateinit var protocolsService: ProtocolsService


    @GetMapping("/results/all")
    fun storeAllResults() {
        service.getAllResults()
    }

    @GetMapping("/protocols/all")
    fun storeAllProtocols() {
        protocolsService.getAllProtocols()
    }

}