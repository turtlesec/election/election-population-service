package no.election.populationservice

import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus.valueOf
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.status
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.zalando.problem.Problem
import org.zalando.problem.ThrowableProblem

@RestControllerAdvice
class ExceptionHandler {

    private val logger = LoggerFactory.getLogger(javaClass)

    @ExceptionHandler(ThrowableProblem::class)
    fun global(problem: Problem): ResponseEntity<Problem> {
        logger.info(problem.detail)
        return status(valueOf(problem.status!!.statusCode)).body(problem)
    }
}