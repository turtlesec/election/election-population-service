package no.election.populationservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PopulationServiceApplication

fun main(args: Array<String>) {
	runApplication<PopulationServiceApplication>(*args)
}
