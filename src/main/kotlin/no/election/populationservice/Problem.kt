package no.election.populationservice

class Problem(
        val title: String,
        val status: Int,
        val detail: String?,
        val code: String?
)
