package no.election.populationservice

import no.election.populationservice.dto.*
import no.election.populationservice.model.Deviation
import no.election.populationservice.model.Protocol
import no.election.populationservice.model.Result
import org.apache.commons.lang3.StringUtils
import org.zalando.problem.Problem
import org.zalando.problem.Status
import org.zalando.problem.ThrowableProblem
import kotlin.math.absoluteValue
import kotlin.reflect.full.memberProperties

class Util {
    companion object {

        fun List<Result>.toElectionEntityList() = this.map {
            ElectionEntity(
                    name = StringUtils.stripAccents(it.name),
                    county = it.parent!!,
                    nr = it.nr,
                    originalName = it.originalName,
                    parentOriginalName = it.parentOriginalName!!
            )
        }

        fun ResultDto.toResult() = with(::Result) {
            val propertiesByName = ResultDto::class.memberProperties.associateBy { it.name }
            callBy(parameters.associateWith { parameter ->
                when (parameter.name) {
                    no.election.populationservice.model.Result::parent.name -> "${links.up.name}".spaceToUnderscore().stripAccents().toLowerCase().norwegianToEnglishCharacters()
                    no.election.populationservice.model.Result::parentOriginalName.name -> "${links.up.name}"
                    no.election.populationservice.model.Result::name.name -> identifier.name.spaceToUnderscore().toLowerCase().norwegianToEnglishCharacters()
                    no.election.populationservice.model.Result::originalName.name -> identifier.name
                    no.election.populationservice.model.Result::level.name -> identifier.level.toLowerCase()
                    no.election.populationservice.model.Result::nr.name -> identifier.nr
                    no.election.populationservice.model.Result::year.name -> identifier.electionYear
                    no.election.populationservice.model.Result::electionLevel.name -> identifier.electionType.toLowerCase()
                    else -> propertiesByName[parameter.name]?.get(this@toResult)
                }
            })
        }

        private fun String.spaceToUnderscore() =
                this.replace(" ", "_")

        private fun String.norwegianToEnglishCharacters() =
                this.replace("å", "a")
                        .replace("æ", "a")
                        .replace("ø", "o")

        private fun String.stripAccents() =
                StringUtils.stripAccents(this)
        fun Int.toProblem(): ThrowableProblem = Problem.valueOf(Status.valueOf(this))
        fun DeviationDto.toDeviation(id: String, name: String) = with(::Deviation) {
            val propertiesByName = DeviationDto::class.memberProperties.associateBy { it.name }
            callBy(parameters.associateWith { parameter ->
                when (parameter.name) {
                    Deviation::name.name -> name
                    else -> propertiesByName[parameter.name]?.get(this@toDeviation)
                }
            })
        }

        fun ProtocolDto.toProtocol(id: String) = with(::Protocol) {
            val propertiesByName = ProtocolDto::class.memberProperties.associateBy { it.name }
            callBy(parameters.associateWith { parameter ->
                when (parameter.name) {
                    Protocol::entityNr.name -> id
                    Protocol::absoluteDeviation.name -> numbers.absoluteDeviation()
                    Protocol::reportedDeviation.name -> numbers.reportedDeviation()
                    else -> propertiesByName[parameter.name]?.get(this@toProtocol)
                }
            })
        }

        fun Numbers.absoluteDeviation() = this.deviation.map {
            if (it.key != "Totalt antall partifordelte stemmesedler") {
                it.value.deviation.absoluteValue
            } else {
                0
            }
        }.sum()

        fun Numbers.reportedDeviation() = this.deviation.filter { it.key == "Totalt antall partifordelte stemmesedler" }.toList().first().second.deviation

    }
}