package no.election.populationservice.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import no.election.populationservice.model.Deviation
import no.election.populationservice.model.Protocol
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import kotlin.math.absoluteValue
import kotlin.reflect.full.memberProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class ProtocolDto(
        val electionYear: String,
        val downloadTime: String?,
        val election: String,
        val municipality: String,
        val county: String,
        val numbers: Numbers,
        var entityNr: String?
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class Numbers(
        @JsonProperty("D2.4 Avvik mellom foreløpig og endelig opptelling av ordinære valgtingsstemmesedler")
        val deviation: Map<String, DeviationDto>
)

data class DeviationDto(
        @JsonProperty("Foreløpig") val preliminary: Int,
        @JsonProperty("Endelig") val final: Int,
        @JsonProperty("Avvik") val deviation: Int
)


