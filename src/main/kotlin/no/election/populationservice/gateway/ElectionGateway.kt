package no.election.populationservice.gateway

import no.election.populationservice.Util.Companion.toProblem
import no.election.populationservice.dto.ResultDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.lang.Exception
import javax.ws.rs.client.Client
import javax.ws.rs.core.GenericType
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Component
class ElectionGateway {

    @Autowired
    private lateinit var electionClient: Client


    fun getElectionResult(href: String): ResultDto {
        return handleResponse(
                electionClient
                        .target("https://valgresultat.no/api")
                        .path(href)
                        .request()
                        .accept(MediaType.APPLICATION_JSON_TYPE)
                        .get()
        )
    }

    fun handleResponse(response: Response): ResultDto {
        return when (response.status) {
            200,269 -> response.readEntity(object : GenericType<ResultDto>() {})
            else -> throw response.status.toProblem()
        }
    }
}

