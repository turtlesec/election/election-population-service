package no.election.populationservice.gateway

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import no.election.populationservice.Util.Companion.toProblem
import no.election.populationservice.dto.ProtocolDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.GenericType
import javax.ws.rs.core.MediaType

@Component
class GitHubGateway {

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var gitHubClient: WebTarget

    fun getElectionProtocol(county: String, municipality: String): ProtocolDto {
        val response = gitHubClient
                .path(county)
                .path("${municipality}.json")
                .request()
                .accept(MediaType.APPLICATION_JSON_TYPE)
                .get()

        return when (response.status) {
            200 -> objectMapper.readValue(response.readEntity(object : GenericType<String>() {}))
            else -> throw response.status.toProblem()
        }
    }
}


