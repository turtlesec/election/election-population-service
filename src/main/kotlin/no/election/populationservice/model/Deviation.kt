package no.election.populationservice.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
data class Deviation(
        val name: String,
        val preliminary: Int,
        val final: Int,
        val deviation: Int
) {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    val id: Int = 0
}