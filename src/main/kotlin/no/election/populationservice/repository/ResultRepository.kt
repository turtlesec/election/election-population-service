package no.election.populationservice.repository

import no.election.populationservice.model.Result
import no.election.populationservice.dto.Parties
import no.election.populationservice.dto.PartyIdentifier
import no.election.populationservice.model.Protocol
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.PagingAndSortingRepository
import java.util.*

interface ResultRepository: PagingAndSortingRepository<Result, Int> {
    fun findByNameAndParent(name:String, parent:String) : Optional<Result>
    fun findByNameAndLevel(name:String, level:String="kommune") : List<Result>
    fun findAllByParties(parties: Parties) : List<Result>
    fun findAllByLevel(level: String) : List<Result>
    fun findAllByParentAndLevel(parent: String, level: String) : List<Result>
    fun findByNr(nr: String) : Optional<Result>
    fun findByLevelAndParentContaining(level: String = "kommune", parent: String, pageable: Pageable) : Page<Result>
}

interface PartyRepository: JpaRepository<Parties, Int> {
    fun findAllByPartyIdentifier(identifier: PartyIdentifier) : List<Parties>
}
interface PartyIdRepository: JpaRepository<PartyIdentifier, Int> {
    fun findAllByCodeEquals(code: String) : List<PartyIdentifier>
}

interface ProtocolRepository: PagingAndSortingRepository<Protocol, Int> {
    fun findByEntityNr(entityNr: String) : Optional<Protocol>
}
