package no.election.populationservice.service

import no.election.populationservice.Util.Companion.toElectionEntityList
import no.election.populationservice.Util.Companion.toResult
import no.election.populationservice.dto.ElectionEntity
import no.election.populationservice.dto.ResultDto
import no.election.populationservice.gateway.ElectionGateway
import no.election.populationservice.model.Result
import no.election.populationservice.repository.ResultRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ElectionService {

    val topLevel: List<String> = listOf("land", "fylke", "kommune")

    @Autowired
    lateinit var electionGateway: ElectionGateway

    @Autowired
    lateinit var resultRepository: ResultRepository

    fun getAndPut(href: String): ResultDto {
        var electionDto: ResultDto?
        do {
            electionDto = electionGateway.getElectionResult(href)
        } while (!isEligible(electionDto!!))
        saveOrUpdate(electionDto.toResult())
        return electionDto
    }

    fun getAllResults() {
        getAndPut("2019/ko").links.related.forEach { county ->
            getAndPut(county.href).links.related.forEach { municipality ->
                getAndPut(municipality.href)
                //TODO districts
                // .links.related.forEach { district-> getAndPut(district.href) }
            }
        }
    }

    fun isEligible(result: ResultDto): Boolean {
        return when {
            !isTopLevel(result) -> true
            !containsNulls(result) -> true
            else -> false
        }
    }

    fun isTopLevel(result: ResultDto) = result.identifier.level in topLevel

    fun containsNulls(result: ResultDto) = result.parties.first().mandates?.mandateResult?.mandateChange == null

    fun saveOrUpdate(entity: Result) {
        resultRepository.findByNameAndParent(entity.name, entity.level).ifPresent {
            resultRepository.delete(it)
        }
        resultRepository.save(entity)

    }

    fun getAllEntities(): List<ElectionEntity> {
        return resultRepository.findAllByLevel("kommune").toElectionEntityList()
    }
}

