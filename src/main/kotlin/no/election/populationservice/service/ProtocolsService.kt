package no.election.populationservice.service

import no.election.populationservice.Util.Companion.toProtocol
import no.election.populationservice.gateway.GitHubGateway
import no.election.populationservice.model.Protocol
import no.election.populationservice.repository.ProtocolRepository
import no.election.populationservice.repository.ResultRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.zalando.problem.ThrowableProblem

@Service
class ProtocolsService {

    private val logger = LoggerFactory.getLogger(javaClass)

    @Autowired
    lateinit var protocolGateway: GitHubGateway

    @Autowired
    lateinit var protocolRepository: ProtocolRepository

    @Autowired
    lateinit var resultRepository: ResultRepository

    @Autowired
    lateinit var electionService: ElectionService


    fun getAllProtocols() {
        electionService.getAllEntities().forEach {
            try {
                protocolRepository.save(
                        protocolGateway.getElectionProtocol(county = it.parentOriginalName, municipality = it.originalName).toProtocol(it.nr)
                )
                logger.info("${it.parentOriginalName}/${it.originalName} found")
            } catch (p: ThrowableProblem) {
                logger.info("${it.parentOriginalName}/${it.originalName} not found")
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }


    fun getAllProtocols(municipality: String, county: String): Protocol {
        val entity = resultRepository.findByNameAndParent(name = municipality, parent = county).orElseThrow()
        val toSave = protocolGateway.getElectionProtocol(entity.parentOriginalName!!, entity.originalName).toProtocol(entity.nr)
        protocolRepository.save(toSave)
        val protocol = protocolRepository.findByEntityNr(entity.nr).orElseThrow()
        return protocol
    }
}